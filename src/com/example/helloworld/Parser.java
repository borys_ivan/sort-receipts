package com.example.helloworld;

import java.io.*;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;
import java.net.HttpURLConnection;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.trim;

public class Parser {

    public static Receipt [] receiptsData;

    private static String lineRead;
    private static String dataAPI;
    private static String[] receipts;
    private static boolean check;
    private static ArrayList<String> result = new ArrayList<String>();

    private static PropertyAccess github=new PropertyAccess();

    public ArrayList<String> ArrayDump(String IdIssue) throws Exception {

        try {

            BufferedReader httpResponseReader = null;
            URL myURL = new URL("https://api.github.com/repos/Expensify/Email-Parsers/issues/" + IdIssue);
            HttpURLConnection myURLConnection = (HttpURLConnection) myURL.openConnection();

            //String userCredentials = "username:pass";
            //String userCredentials = "token 0d569fc5c1261bae93416e0030573d04fa1f235c";
            //String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

            //myURLConnection.setRequestProperty ("Authorization", basicAuth);
            myURLConnection.setRequestProperty("Authorization", github.key().get("github").toString());
            myURLConnection.setRequestMethod("GET");
            myURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            myURLConnection.setRequestProperty("Content-Language", "en-US");
            myURLConnection.setUseCaches(false);
            myURLConnection.setDoInput(true);
            myURLConnection.setDoOutput(true);

            httpResponseReader =
                    new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));


            while ((lineRead = httpResponseReader.readLine()) != null) {

                Document doc = Jsoup.parse(lineRead);
                JSONObject json = new JSONObject(doc.getElementsByTag("body").text());
                dataAPI = json.getString("body");
//////////////////
                receiptsData = getReceipt(dataAPI);
/////////////////////////

                Pattern p = Pattern.compile("https://");
                receipts = dataAPI.split("\\[\n '|',");

                for (String receipt : receipts) {

                    check = p.matcher(receipt).find();
                    if (check) {
                        result.add(trim(receipt));
                    }
                }
            }

        }catch (Exception e){
            System.out.println("ID issue don't exist in this list issues");
        }

            return result;

    }

    private Receipt[] getReceipt(String dataAPI) {

        String[] arrReceipt, amountReq, currencyReq, dateReq,extensionReq;
        String amount, currency, date, extension;
        int i = 0;

        Pattern p = Pattern.compile("=>");
        receipts = dataAPI.split("\\s\\[\\n\\s\\[|\\s\\]\\,\\n\\s\\]\\,");
        Receipt[] receiptsIn = new Receipt[receipts.length];

        for (String receipt : receipts) {

            check = p.matcher(receipt).find();
            arrReceipt = receipt.split(",");

            if (check) {

                amount = trim(arrReceipt[0]);
                currency = trim(arrReceipt[1]);
                date = trim(arrReceipt[2]);
                extension=trim(arrReceipt[3]);
//System.out.println(date);
                amountReq = amount.split("'amount' => ");
                currencyReq = currency.split("'currency' => ");
                dateReq = date.split("'date' => ");
                extensionReq=extension.split("'extension' => ");

                receiptsIn[i] = new Receipt(amountReq[1], currencyReq[1], dateReq[1],extensionReq[1]);

                i++;
            }

        }

        return receiptsIn;
    }

}




