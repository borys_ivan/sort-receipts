package com.example.helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.*;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.regex.*;

import com.example.helloworld.WriteDate;


public class SortDate {

    private HashMap WrongDate = new HashMap<>();
    private HashMap totalWithoutReqex = new HashMap<>();
    private HashMap totalRsults = new HashMap<>();
    private HashMap<String, String> checkDate = new HashMap<>();
    private static PropertyAccess vagrant = new PropertyAccess();

    public static void main(String[] args) throws Exception {


        String IdIssue;
        boolean check = false;

        do {
            Scanner in = new Scanner(System.in);
            System.out.print("ID issue github:#");
            IdIssue = in.nextLine();

            if (IdIssue == "locale") {
                check = true;
            }

            if (!IdIssue.isEmpty() && IdIssue.length() == 5 && IdIssue.matches("[0-9]+")) {
                //if(!IdIssue.isEmpty() && IdIssue.length() == 6){
                check = true;
            } else {
                System.out.println(String.format("ID Issue is incorrect"));
            }

        } while (!check);


        if (check) {

            SortDate start = new SortDate();
            /*Для парсінга з сторінки github*/
            ArrayList Receipts = new Parser().ArrayDump(IdIssue);
            /*Для збереженої сторінки github*/
            //ArrayList Receipts = new Html().ArraryReceipt();

            Dump[] ReceiptsDump = start.connectToMachine(Receipts);
            Object Result = new Object();
            double amount = ReceiptsDump.length;


            int count = 0;
            int procent = 50 - ReceiptsDump.length;

            if (procent >= 0) {
                count = procent * 2;
            }


            compareData(Parser.receiptsData, ReceiptsDump);


            if (Receipts.size() != 0) {

                for (Dump receipt : ReceiptsDump) {

                    Result = start.dumpRegex(receipt.getReceipt(), receipt.getDump());

                    count += 100 / amount;
                    progress(count, amount);

                }

                System.out.println("Processing: Done!          ");

                createTable(((MyResult) Result).getWithRegex(), Receipts);
                createTable(((MyResult) Result).getWithoutRegex(), Receipts);

            } else {

                System.out.println("Processing: Stop!          ");

            }


        }

    }


    private static void progress(int count, double amount) {

        char[] animationChars = new char[]{'|', '/', '-', '\\'};

        System.out.print("\r" + "Processing: " + count + "% " + animationChars[count % 3]);
        System.out.flush();

        try {
            Thread.sleep(Math.round(amount) * 3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void createTable(HashMap Result, ArrayList Receipts) {

        StringBuilder s = new StringBuilder();
        int count = Receipts.get(0).toString().length();
        String equalseline = "=";
        String equalsePlus = "";
        final String equalseLongLine;
        String line = "-";
        String Plus = "";
        final String LongLine;

        for (int i = 0; i <= count; i++) {
            equalsePlus += equalseline;
            Plus += line;
        }

        equalseLongLine = equalsePlus;
        LongLine = Plus;

        Result.forEach((l, d) -> {
            s.append(String.format("\n" + equalseLongLine + "\n"));
            s.append(l);
            s.append(String.format("\n" + LongLine));
            s.append(d);
            s.append(String.format("\n" + equalseLongLine + "\n"));
        });

        System.out.println(s.toString());

    }


    public Dump[] connectToMachine(ArrayList Receipts) {

        LinkedHashMap<String, StringBuffer> dumpMapping = new LinkedHashMap<>();
        Dump[] dump = new Dump[Receipts.size()];
        int count = 0;

        try {

            String commandOn = "cd /vagrant/Email-Parsers && php cli.php dump ";
            String host = "127.0.0.1";
            String user = "vagrant";
            //String password = "vagrant";
            String absoluteFilePathPrivatekey = vagrant.key().get("vagrant").toString();

            JSch jsch = new JSch();
            jsch.addIdentity(absoluteFilePathPrivatekey);
            Session session = jsch.getSession(user, host, 2222);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);

            //session.setPassword(password);
            session.connect();


            //((ChannelExec)channel).setCommand(commandOn);
            //---------------------------------------------------------------

            for (Object receipt : Receipts) {

                Channel channel = session.openChannel("exec");
                ((ChannelExec) channel).setCommand(commandOn + receipt.toString());

                //System.out.println("Channel Connected to machine " + host + " server with command: " + CommandDump[i] );

                channel.setInputStream(null);
                ((ChannelExec) channel).setErrStream(System.err);

                InputStream input = channel.getInputStream();
                channel.connect();

                //System.out.println("Channel Connected to machine " + host + " server with command: " + CommandDump[i]);


                InputStreamReader inputReader = new InputStreamReader(input);
                BufferedReader bufferedReader = new BufferedReader(inputReader);
                String line = null;


                StringBuffer s = new StringBuffer();
                while ((line = bufferedReader.readLine()) != null) {
                    //s.append(line + "\n");
                    s.append(line + " ");

                }

                dump[count] = new Dump(receipt.toString(), s);

                count++;
            }

            //channel.disconnect();
            session.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dump;
    }


    private MyResult dumpRegex(Object receipt, Object dump) {

        HashMap<String, Boolean> withoutRegex = new HashMap<>();
        HashMap<String, String> withRegex = new HashMap<>();
        int count = 0;
        boolean result;
        String[] dateReqex = Regex.data;


        do {

            Pattern p = Pattern.compile(dateReqex[count], Pattern.CASE_INSENSITIVE);
            Matcher m = p.matcher(dump.toString());
            result = m.find();

            if (result) {

                withRegex = addToArrayRegex(dateReqex[count], receipt.toString());

                withoutRegex.replace(receipt.toString(), false);

                break;

            } else {

                withoutRegex.put(receipt.toString(), true);

            }

            count++;

        } while (count < dateReqex.length);


        totalRsults = withRegex;
        totalWithoutReqex = withoutRegex;


        totalWithoutReqex.forEach((l, d) -> {
            if (d.equals(true)) {

                if (WrongDate.get("another date") == null) {

                    WrongDate.put("another date", "\n" + l);

                } else {

                    WrongDate.put("another date", WrongDate.get("another date") + "\n" + l);
                }
            }
        });

        return new MyResult(totalRsults, WrongDate);
    }


    private HashMap addToArrayRegex(String regex, String receipt) {

        String check = checkDate.get(regex);

        if (check == null) {
            checkDate.put(regex, "\n" + receipt);
        }

        if (check != null) {
            checkDate.put(regex, checkDate.get(regex) + "\n" + receipt);
        }

        return checkDate;
    }


    private static void compareData(Receipt[] data, Dump[] dump) {

        int count = 0;
        boolean checkCurrency, checkAmount, checkDate;
        String checkExtension;
        String[] lineAmount = new String[dump.length];
        String[] lineCurrency = new String[dump.length];
        String[] receiptUrl = new String[dump.length];
        String[] lineDate = new String[dump.length];
        String[] lineExtension = new String[dump.length];
        String replaceAmount;

        do {

            Pattern amount = Pattern.compile(data[count].getAmount());
            Pattern currency = Pattern.compile(data[count].getCurrency().replace("\'", ""), Pattern.CASE_INSENSITIVE);
            Pattern date = Pattern.compile(data[count].getDate());

            replaceAmount = dump[count].getDump().toString().replaceAll("([\\d]+)(?:\\,|\\.)([\\d]{2})", "$1$2");
            checkAmount = amount.matcher(replaceAmount).find();
            checkCurrency = currency.matcher(dump[count].getDump()).find();
            checkDate = date.matcher(dump[count].getDump()).find();
            checkExtension = data[count].getExtension();

            receiptUrl[count] = dump[count].getReceipt();

            if (checkAmount) {
                lineAmount[count] = "'amount' => " + data[count].getAmount() + ",";
            } else {
                lineAmount[count] = "'amount' => " + data[count].getAmount() + ",//different amount";
            }

            if (checkCurrency) {
                lineCurrency[count] = "'currency' => " + data[count].getCurrency() + ",";
            } else {
                lineCurrency[count] = "'currency' => null,";
            }

            if (checkDate) {
                lineDate[count] = "'date' => " + data[count].getDate() + ",";
            } else {
                lineDate[count] = "'date' => " + data[count].getDate() + ",";
            }

            if (checkExtension != null) {
                lineExtension[count] = "'extension' => " + data[count].getExtension() + ",";
            }

            count++;

        } while (count < dump.length);


        WriteDate write = new WriteDate();

        try {
            write.write(receiptUrl, lineAmount, lineCurrency, lineDate, lineExtension);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}


final class MyResult {
    private final HashMap withRegex;
    private final HashMap withoutRegex;

    public MyResult(HashMap withRegex, HashMap withoutRegex) {
        this.withRegex = withRegex;
        this.withoutRegex = withoutRegex;
    }

    public HashMap getWithRegex() {
        return withRegex;
    }

    public HashMap getWithoutRegex() {
        return withoutRegex;
    }
}


final class Dump {
    private String receipt;
    private StringBuffer dump;

    public Dump(String receipt, StringBuffer dump) {

        this.receipt = receipt;
        this.dump = dump;
    }

    public String getReceipt() {
        return receipt;
    }

    public StringBuffer getDump() {
        return dump;
    }

}

