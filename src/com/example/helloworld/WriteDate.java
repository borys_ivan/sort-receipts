package com.example.helloworld;


import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.Writer;


public class WriteDate {


    public void write(String[] receiptUrl, String[] lineAmount, String[] lineCurrency, String[] lineDate, String[] lineExtension) throws IOException {

        File f = new File("/home/iborys/java/writeData/receipts.txt");

        if(f.exists()){
            f.delete();
        }

        Writer writer = new FileWriter(f);

        for (int i = 0; i < receiptUrl.length; i++) {

            String test = "\n[\n    '" + receiptUrl[i] + "',\n    [\n        [\n            " + lineAmount[i] + "\n" + "            " + lineCurrency[i] +
                    "\n            " + lineDate[i] + "\n            " + lineExtension[i] + "\n        ],\n    ],\n],";

            writer.write(test);
            writer.flush();
        }

        writer.close();
    }


}
