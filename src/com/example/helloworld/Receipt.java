package com.example.helloworld;

public class Receipt {

    private String amount;
    private String currency;
    private String date;
    private String extension;

    public Receipt(String amount, String currency, String date, String extension) {

        this.amount = amount;
        this.currency = currency;
        this.date = date;
        this.extension = extension;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }


    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getDate() {
        return date;
    }

    public String getExtension() {
        return extension;
    }

}
