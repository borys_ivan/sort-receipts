package com.example.helloworld;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.trim;


public class Html {

    private static String[] receipts;
    private static boolean check;
    private static ArrayList<String> result = new ArrayList<String>();

    public ArrayList<String> ArraryReceipt() throws Exception {

        File input = new File("/home/iborys/java/page/22776.html");
        Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");

        System.out.println(doc.title());

        Elements LinksReceipts = doc.getElementsByClass("edit-comment-hide");
        receipts = LinksReceipts.toString().split("<code>\\[|\\[\n.*?'|',|'");

        Pattern p = Pattern.compile("https://");

        for (String receipt : receipts) {

            check = p.matcher(receipt).find();

            if (check) {
                result.add(trim(receipt));
            }
        }
        return result;
    }

}
