package com.example.helloworld;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

public class PropertyAccess {

    private  String github;
    private  String vagrant;
    private  HashMap access = new HashMap<>();

    public HashMap key() throws Exception{

        File file = new File("/home/iborys/java/resource/key.config");
        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;

        while ((st = br.readLine()) != null) {
            JSONObject json = new JSONObject(st);
            access.put("github",json.getString("github_token"));
            access.put("vagrant",json.getString("vagrant_private_key"));
        }

        return access;

    }

}